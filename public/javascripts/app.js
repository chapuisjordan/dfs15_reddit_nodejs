let template = "" ;
fetch('/template/post').then(result => result.text()).then(result => {
  template = result ;
});

const run = () => {
  document.getElementById('search').addEventListener('submit', event => {
    event.preventDefault();
    event.stopPropagation();
    const search = event.target.querySelector('input[type=search]') ;
    fetch('/posts?search='+search.value).then(result => result.json()).then(result => {
      if(result.ok && result.nbResults) {
        document.getElementById('container').innerHTML = '' ;
        result.result.forEach(l => {
          document.getElementById('container').innerHTML += l ;
        })
      }
      search.value = "";
    })
  })
};



window.addEventListener('DOMContentLoaded',run) ;