var express = require('express');
var router = express.Router();
const mongo = require('../bin/mongo') ;

/* GET home page. */
router.get('/', function(req, res, next) {
  mongo.getInstance().collection('posts').find().limit(50).toArray((err, posts) => {
    res.render('index', { title: 'It-Reddit', posts : posts});
  })
});

module.exports = router;
